//
//  UpstreamMessage.swift
//  
//
//  Created by Filip Klembara on 23/02/2020.
//

public enum UpstreamGroupMessage {
    case remove(groupId: String)
    case add(groupId: String)
    case subscribe(userId: String, role: Role, groupId: String)
    case unsubscribe(userId: String, groupId: String)
}

public enum UpstreamMessage {
    case push(LogCodablePush)
    case group(logId: String, message: UpstreamGroupMessage)
    case pull(request: SyncPullRequest, pull: CodablePull)
}
