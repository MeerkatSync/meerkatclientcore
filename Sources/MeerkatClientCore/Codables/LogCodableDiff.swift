//
//  LogCodableDiff.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

public struct LogCodableDiff {
    public let syncLogId: String
    public let diff: CodableDiff

    public init(syncLogId: String, diff: CodableDiff) {
        self.syncLogId = syncLogId
        self.diff = diff
    }

    public func changeToDefaultGroup(from group: GroupID) -> LogCodableDiff {
        .init(syncLogId: syncLogId, diff: diff.changeToDefaultGroup(from: group))
    }
}

