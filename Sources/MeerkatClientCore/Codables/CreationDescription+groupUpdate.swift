//
//  CodableCreationDescription.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

extension CodableCreationDescription {
    public func changeFromDefaultGroup(to group: GroupID) -> CodableCreationDescription {
        guard groupID == SyncGroupDefaults.defaultName else {
            return self
        }
        return .init(groupID: group, object: object)
    }

    public func changeToDefaultGroup(from group: GroupID) -> CodableCreationDescription {
        guard groupID == group else {
            return self
        }
        return .init(groupID: SyncGroupDefaults.defaultName, object: object)
    }
}
