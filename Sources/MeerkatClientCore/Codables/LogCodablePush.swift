//
//  LogCodablePush.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

public struct LogCodablePush {
    public let syncLogId: String
    public let push: CodablePush

    public init(syncLogId: String, push: CodablePush) {
        self.syncLogId = syncLogId
        self.push = push
    }

    public func changeFromDefaultGroup(to group: GroupID) -> LogCodablePush {
        .init(syncLogId: syncLogId, push: push.changeFromDefaultGroup(to: group))
    }
}
