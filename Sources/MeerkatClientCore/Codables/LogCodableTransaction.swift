//
//  LogCodableTransaction.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

public struct LogCodableTransaction {
    public let syncLogId: String
    public let transaction: CodableTransaction

    public init(syncLogId: String, transaction: CodableTransaction) {
        self.syncLogId = syncLogId
        self.transaction = transaction
    }
}
