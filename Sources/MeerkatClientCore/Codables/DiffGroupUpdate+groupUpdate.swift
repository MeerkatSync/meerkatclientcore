//
//  CodableDiffGroupUpdate.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

extension CodableDiffGroupUpdate {
    public func changeToDefaultGroup(from group: GroupID) -> CodableDiffGroupUpdate {
        guard groupId == group else { return self }
        return .init(groupId: SyncGroupDefaults.defaultName, version: version, role: role)
    }
}
