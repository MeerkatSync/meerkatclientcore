//
//  SyncPullRequestPublisher.swift
//  
//
//  Created by Filip Klembara on 01/03/2020.
//

import Foundation
import Combine

extension SyncPullRequest {
    public final class Publisher<Failure: Error>: Combine.Publisher {
        public func receive<S>(subscriber: S) where S : Subscriber, Failure == S.Failure, Output == S.Input {
            subscriber.receive(subscription: Inner(downstream: subscriber))
        }

        public typealias Output = SyncPullRequest
    }
}
extension SyncPullRequest.Publisher {
    private final class Inner<Downstream: Subscriber>: Subscription where Downstream.Input == SyncPullRequest{

        private enum Status {
            case subscribed
            case cancalled
        }
        private let mutex = DispatchSemaphore(value: 1)
        private var status = Status.subscribed
        private var notifSubscriber: AnyCancellable? = nil
        private var demands: Subscribers.Demand = .none
        private let downstream: Downstream
        private var current: SyncPullRequest? {
            didSet {
                guard current != nil else {
                    return
                }
                if let old = oldValue {
                    old.complete(.noData, by: .meerkat)
                }
                lockedTrySend()
            }
        }

        init(downstream: Downstream) {
            self.downstream = downstream
            notifSubscriber = NotificationCenter.default.publisher(for: .SyncPullRequest).sink(receiveValue: handleNotification(_:))
        }

        private func handleNotification(_ notification: Notification) {
            guard let req = SyncPullRequest(notification: notification) else {
                return
            }
            mutex.wait()
            defer { mutex.signal() }
            current = req
        }

        func request(_ demand: Subscribers.Demand) {
            mutex.wait()
            defer { mutex.signal() }
            demands += demand
            lockedTrySend()
        }

        private func lockedTrySend() {
            guard status == .subscribed else { return }
            guard let req = current else { return }
            guard demands > .none else { return }
            mutex.signal()
            let d = downstream.receive(req)
            mutex.wait()
            demands -= 1
            demands += d
            current = nil
        }

        func cancel() {
            mutex.wait()
            defer { mutex.signal() }
            notifSubscriber?.cancel()
            notifSubscriber = nil
            current?.complete(.noData, by: .meerkat)
            current = nil
        }
    }
}
