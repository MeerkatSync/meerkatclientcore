import XCTest
@testable import MeerkatClientCore

final class MeerkatClientCoreTests: XCTestCase {
    func testFromDefaultGroup() {
        let push = CodablePush(
            schemeVersion: 2,
            groups: [
                .init(id: "groupA", version: 2),
                .init(id: SyncGroupDefaults.defaultName, version: 5),
            ],
            transaction: .init(
                deletions: [
                .init(timestamp: Date(), updates: [], className: "Person", objectId: "Tom"),
                ], creations: [
                    .init(groupID: "groupA", object: .init(timestamp: Date(), updates: [], className: "Person", objectId: "John")),
                    .init(groupID: SyncGroupDefaults.defaultName, object: .init(timestamp: Date(), updates: [], className: "Person", objectId: "John")),
                ], modifications: [
                    .init(timestamp: Date(), updates: [.init(attribute: "age", value: nil)], className: "Person", objectId: "John")
            ]))
        let logPush = LogCodablePush(syncLogId: "logId3", push: push)
        let nLogPush = logPush.changeFromDefaultGroup(to: "groupB")
        XCTAssertEqual(nLogPush.syncLogId, "logId3")
        let nPush = nLogPush.push
        XCTAssertEqual(nPush.groups.count, 2)
        
        guard case let (g1?, g2?) = (nPush.groups.first, nPush.groups.last) else {
            XCTFail()
            return
        }

        XCTAssertNotEqual(g1.id, g2.id)
        XCTAssertEqual(g1.id, "groupA")
        XCTAssertEqual(g2.id, "groupB")

        XCTAssertEqual(nPush.transaction.deletions.count, 1)
        XCTAssertEqual(nPush.transaction.modifications.count, 1)
        XCTAssertEqual(nPush.transaction.creations.count, 2)

        guard case let (c1?, c2?) = (nPush.transaction.creations.first, nPush.transaction.creations.last) else {
            XCTFail()
            return
        }

        XCTAssertNotEqual(c1.groupID, c2.groupID)
        XCTAssertEqual(c1.groupID, "groupA")
        XCTAssertEqual(c2.groupID, "groupB")
    }

    func testToDefaultGroup() {
        let diff = CodableDiff(
            transactions: [
                .init(deletions: [], creations: [
                    .init(groupID: "groupA", object: .init(timestamp: Date(), updates: [], className: "Person", objectId: "Lucy")),
                    .init(groupID: "groupB", object: .init(timestamp: Date(), updates: [], className: "Person", objectId: "Janet")),
                ], modifications: []),
                .init(deletions: [], creations: [
                    .init(groupID: "groupB", object: .init(timestamp: Date(), updates: [], className: "Dog", objectId: "Bobby")),
                    .init(groupID: "groupA", object: .init(timestamp: Date(), updates: [], className: "Dog", objectId: "Lobby")),
                    .init(groupID: "groupC", object: .init(timestamp: Date(), updates: [], className: "Dog", objectId: "Tobby")),
                    .init(groupID: "groupA", object: .init(timestamp: Date(), updates: [], className: "Dog", objectId: "Mobby")),
                ], modifications: []),
            ], groups: [
                .init(groupId: "groupB", version: 4, role: .readAndWrite),
                .init(groupId: "groupA", version: 3, role: .owner),
                .init(groupId: "groupC", version: 51, role: .read)
        ])
        let logDiff = LogCodableDiff(syncLogId: "logId41", diff: diff)
        let dLogDiff = logDiff.changeToDefaultGroup(from: "groupA")
        XCTAssertEqual(dLogDiff.syncLogId, "logId41")
        let dDiff = dLogDiff.diff

        XCTAssertEqual(dDiff.groups.count, 3)

        guard dDiff.groups.count == 3 else {
            XCTFail()
            return
        }

        let (g1, g2, g3) = (dDiff.groups[0], dDiff.groups[1], dDiff.groups[2])
        XCTAssertEqual(g1.groupId, "groupB")
        XCTAssertEqual(g1.role, .readAndWrite)
        XCTAssertEqual(g1.version, 4)

        XCTAssertEqual(g2.groupId, SyncGroupDefaults.defaultName)
        XCTAssertEqual(g2.role, .owner)
        XCTAssertEqual(g2.version, 3)

        XCTAssertEqual(g3.groupId, "groupC")
        XCTAssertEqual(g3.role, .read)
        XCTAssertEqual(g3.version, 51)

        XCTAssertEqual(dDiff.transactions.count, 2)

        guard dDiff.transactions.count == 2 else {
            XCTFail()
            return
        }

        let (t1, t2) = (dDiff.transactions[0], dDiff.transactions[1])
        XCTAssertEqual(t1.deletions.count, 0)
        XCTAssertEqual(t1.modifications.count, 0)
        XCTAssertEqual(t1.creations.count, 2)

        guard t1.creations.count == 2 else {
            XCTFail()
            return
        }

        let t1c1 = t1.creations[0]
        let t1c2 = t1.creations[1]

        XCTAssertEqual(t1c1.groupID, SyncGroupDefaults.defaultName)
        XCTAssertEqual(t1c2.groupID, "groupB")

        guard t2.creations.count == 4 else {
            XCTFail()
            return
        }

        let t2c1 = t2.creations[0]
        let t2c2 = t2.creations[1]
        let t2c3 = t2.creations[2]
        let t2c4 = t2.creations[3]

        XCTAssertEqual(t2c1.groupID, "groupB")
        XCTAssertEqual(t2c2.groupID, SyncGroupDefaults.defaultName)
        XCTAssertEqual(t2c3.groupID, "groupC")
        XCTAssertEqual(t2c4.groupID, SyncGroupDefaults.defaultName)
    }

    func testPullFromDefaultGroup() {
        let pull = CodablePull(
            schemeVersion: 2,
            groups: [
                .init(id: "groupA", version: 2),
                .init(id: SyncGroupDefaults.defaultName, version: 5),
            ])
        let nPull = pull.changeFromDefaultGroup(to: "groupC")

        XCTAssertEqual(nPull.groups.count, 2)

        guard case let (g1?, g2?) = (nPull.groups.first, nPull.groups.last) else {
            XCTFail()
            return
        }

        XCTAssertNotEqual(g1.id, g2.id)
        XCTAssertEqual(g1.id, "groupA")
        XCTAssertEqual(g2.id, "groupC")
    }

    func testInitLogTransaction() {
        let log = LogCodableTransaction(syncLogId: "logid141501", transaction: .init(deletions: [], creations: [], modifications: []))
        XCTAssertEqual(log.syncLogId, "logid141501")
        XCTAssertEqual(log.transaction.deletions.count, 0)
        XCTAssertEqual(log.transaction.creations.count, 0)
        XCTAssertEqual(log.transaction.modifications.count, 0)
    }

    func testSyncPullRequestOk() {
        let e1 = expectation(description: "ok")
        let e2 = expectation(description: "fail")
        e2.isInverted = true
        func comp(r: NotificationFetchResult) {
            guard r == .newData else {
                e2.fulfill()
                return
            }
            e1.fulfill()
        }
        let req = SyncPullRequest(completion: comp)
        req.complete(.noData, by: .user)
        req.complete(.newData, by: .meerkat)
        wait(for: [e1, e2], timeout: 0.05)
    }

    func testSyncPullRequestFail() {
        let e1 = expectation(description: "ok")
        let e2 = expectation(description: "fail")
        e2.isInverted = true
        func comp(r: NotificationFetchResult) {
            guard r == .failed else {
                e2.fulfill()
                return
            }
            e1.fulfill()
        }
        let req = SyncPullRequest(completion: comp)
        req.complete(.failed, by: .meerkat)
        req.complete(.noData, by: .user)
        wait(for: [e1, e2], timeout: 0.05)
    }

    func testSyncPullRequestNotificationFail() {
        let e1 = expectation(description: "ok")
        let e2 = expectation(description: "fail")
        e2.isInverted = true
        let cancallable = SyncPullRequest.publisher.sink { req in
            req.complete(.failed, by: .meerkat)
        }
        defer { cancallable.cancel() }
        func comp(r: NotificationFetchResult) {
            guard r == .failed else {
                e2.fulfill()
                return
            }
            e1.fulfill()
        }
        let s = SyncPullRequest(completion: comp)
        s.complete(.noData, by: .user)
        s.send()
        wait(for: [e1, e2], timeout: 0.1)
    }

    func testSyncPullRequestNotificationOk() {
        let e1 = expectation(description: "ok")
        let e2 = expectation(description: "fail")
        let e3 = expectation(description: "fail init req")
        e2.isInverted = true
        e3.isInverted = true
        let cancallable = SyncPullRequest.publisherWithError(of: Error.self).sink(receiveCompletion: { _ in
            e3.fulfill()
        }) { (req) in
            req.complete(.noData, by: .meerkat)
        }
        defer { cancallable.cancel() }
        func comp(r: NotificationFetchResult) {
            guard r == .noData else {
                e2.fulfill()
                return
            }
            e1.fulfill()
        }
        let s = SyncPullRequest(completion: comp)
        s.complete(.noData, by: .user)
        s.send()
        wait(for: [e1, e2, e3], timeout: 0.1)
    }

    func testSyncPullRequestNotificationEmptyUserInfo() {
        let e1 = expectation(description: "ok")
        let e2 = expectation(description: "fail")
        e2.isInverted = true
        e1.isInverted = true

        let cancallable = SyncPullRequest.publisher.sink { req in
            e1.fulfill()
        }
        defer { cancallable.cancel() }
        NotificationCenter.default.post(name: .SyncPullRequest, object: nil, userInfo: nil)
        wait(for: [e1, e2], timeout: 0.1)
    }

    func testWrapNotification() {
        let root: [String: Bool] = [SyncPullRequest.remoteNotificationKey: true]
        let userInfo = [SyncPullRequest.remoteNotificationRootKey: root]
        let e1 = expectation(description: "ok sync")
        let e2 = expectation(description: "ok all")

        let cancallable = SyncPullRequest.publisher.sink { req in
            defer { e1.fulfill() }
            req.complete(.noData, by: .meerkat)
        }
        defer { cancallable.cancel() }

        let handler = SyncPullRequest.wrapRemoteNotification(from: userInfo) { res in
            guard case .newData = res else { return }
            e2.fulfill()
        }
        handler(.newData)
        wait(for: [e1, e2], timeout: 0.1)
    }

    func testWrapFetch() {
        let e1 = expectation(description: "ok sync")
        let e2 = expectation(description: "ok all")

        let cancallable = SyncPullRequest.publisher.sink { req in
            defer { e1.fulfill() }
            req.complete(.failed, by: .meerkat)
        }
        defer { cancallable.cancel() }

        let handler = SyncPullRequest.wrapFetch { res in
            guard case .failed = res else { return }
            e2.fulfill()
        }
        handler(.noData)
        wait(for: [e1, e2], timeout: 0.1)
    }

    func testWrapEmptyNotification() {
        let userInfo = [SyncPullRequest.remoteNotificationKey: "Bob"]
        let e1 = expectation(description: "err sync")
        e1.isInverted = true
        let e2 = expectation(description: "ok all")

        let cancallable = SyncPullRequest.publisher.sink { _ in
            e1.fulfill()
        }
        defer { cancallable.cancel() }

        let handler = SyncPullRequest.wrapRemoteNotification(from: userInfo) { res in
            guard case .failed = res else { return }
            e2.fulfill()
        }
        handler(.failed)
        wait(for: [e1, e2], timeout: 0.1)
    }
    
    static var allTests = [
        ("testFromDefaultGroup", testFromDefaultGroup),
        ("testToDefaultGroup", testToDefaultGroup),
        ("testPullFromDefaultGroup", testPullFromDefaultGroup),
        ("testInitLogTransaction", testInitLogTransaction),
        ("testSyncPullRequestOk", testSyncPullRequestOk),
        ("testSyncPullRequestFail", testSyncPullRequestFail),
        ("testSyncPullRequestNotificationFail", testSyncPullRequestNotificationFail),
        ("testSyncPullRequestNotificationOk", testSyncPullRequestNotificationOk),
        ("testSyncPullRequestNotificationEmptyUserInfo", testSyncPullRequestNotificationEmptyUserInfo),
        ("testWrapNotification", testWrapNotification),
        ("testWrapEmptyNotification", testWrapEmptyNotification),
        ("testWrapFetch", testWrapFetch),
    ]
}
