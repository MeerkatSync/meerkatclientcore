//
//  SyncPullRequest.swift
//  
//
//  Created by Filip Klembara on 01/03/2020.
//

import Foundation

public struct SyncPullRequest {
    private static let userInfoSyncPullRequestKey = "syncPullRequest"
    private let completion: (NotificationFetchResult) -> Void
    private final class State {
        enum CompletionStatus {
            case incomplete
            case completedByUser(result: NotificationFetchResult)
            case completedByMeerkat(result: NotificationFetchResult)
            case completed(result: NotificationFetchResult)
        }
        var status = CompletionStatus.incomplete
        var sent: Bool = false
    }

    private let state: State
    public enum Actor {
        case user
        case meerkat
    }
    private let mutex: DispatchSemaphore

    public init?(notification: Notification) {
        guard let userInfo = notification.userInfo, let req = userInfo[SyncPullRequest.userInfoSyncPullRequestKey] as? SyncPullRequest else {
            return nil
        }
        self = req
    }

    init(completion: @escaping (NotificationFetchResult) -> Void) {
        state = State()
        mutex = DispatchSemaphore(value: 1)
        self.completion = completion
    }

    private func finalResult(a: NotificationFetchResult, b: NotificationFetchResult) -> NotificationFetchResult {
        if [a, b].contains(.failed) {
            return .failed
        }
        if [a, b].contains(.newData) {
            return .newData
        }
        return .noData
    }

    public func complete(_ completionStatus: NotificationFetchResult, by actor: Actor) {
        mutex.wait()
        defer { mutex.signal() }

        switch (actor, state.status) {
        case (.meerkat, .incomplete):
            state.status = .completedByMeerkat(result: completionStatus)
        case (.meerkat, .completedByUser(let res)):
            state.status = .completed(result: finalResult(a: completionStatus, b: res))
        case (.meerkat, _):
            assertionFailure("Multiple completion by actor 'Meerkat'")
            return
        case (.user, .incomplete):
            state.status = .completedByUser(result: completionStatus)
        case (.user, .completedByMeerkat(let res)):
            state.status = .completed(result: finalResult(a: completionStatus, b: res))
        case (.user, _):
            assertionFailure("Multiple completion by actor 'User'")
            return
        }
        guard case let .completed(result) = state.status else {
            return
        }
        completion(result)
    }

    func send() {
        mutex.wait()
        guard !state.sent else {
            mutex.signal()
            assertionFailure("Request should be sent obly once")
            return
        }
        state.sent = true
        mutex.signal()
        let userInfo: [AnyHashable: Any] = [
            SyncPullRequest.userInfoSyncPullRequestKey: self
        ]
        NotificationCenter.default.post(name: .SyncPullRequest, object: nil, userInfo: userInfo)
    }
}

extension SyncPullRequest {
    public static var publisher: Publisher<Never> {
        Publisher<Never>()
    }

    public static func publisherWithError<E>(of type: E.Type = E.self) -> Publisher<E> {
        Publisher<E>()
    }
}
