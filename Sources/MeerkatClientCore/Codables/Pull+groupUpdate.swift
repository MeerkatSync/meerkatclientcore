//
//  Pull+groupUpdate.swift
//  
//
//  Created by Filip Klembara on 01/03/2020.
//

extension CodablePull {
    public func changeFromDefaultGroup(to group: GroupID) -> CodablePull {
        .init(schemeVersion: schemeVersion, groups: groups.map { $0.changeFromDefaultGroup(to: group) })
    }
}
