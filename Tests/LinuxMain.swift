import XCTest

import MeerkatClientCoreTests

var tests = [XCTestCaseEntry]()
tests += MeerkatClientCoreTests.allTests()
XCTMain(tests)
