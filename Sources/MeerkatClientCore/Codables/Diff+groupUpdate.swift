//
//  LogCodable.swift
//  
//
//  Created by Filip Klembara on 19/02/2020.
//

extension CodableDiff {
    public func changeToDefaultGroup(from group: GroupID) -> CodableDiff {
        .init(transactions: transactions.map { $0.changeToDefaultGroup(from: group) },
              groups: groups.map { $0.changeToDefaultGroup(from: group) })
    }
}
