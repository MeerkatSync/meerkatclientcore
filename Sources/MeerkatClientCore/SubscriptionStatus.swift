//
//  SubscriptionStatus.swift
//  
//
//  Created by Filip Klembara on 18/02/2020.
//

import Combine

public enum SubscriptionStatus {
    case terminated
    case awaitingSubscription
    case subscribed(subscription: Subscription)
}
