//
//  CodableGroup.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

extension CodableGroup {
    public func changeFromDefaultGroup(to group: GroupID) -> CodableGroup {
        guard id == SyncGroupDefaults.defaultName else {
            return self
        }
        return .init(id: group, version: version)
    }
}

