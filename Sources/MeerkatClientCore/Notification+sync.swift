//
//  Notification+sync.swift
//  
//
//  Created by Filip Klembara on 01/03/2020.
//

import Foundation

extension Notification.Name {
    static let SyncPullRequest = Notification.Name("sync_pull_request")
}

public enum NotificationFetchResult {
    case newData
    case noData
    case failed
}
