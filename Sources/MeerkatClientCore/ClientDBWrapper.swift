//
//  ClientDBWrapper.swift
//  
//
//  Created by Filip Klembara on 28/02/2020.
//

import Combine

public protocol ClientDBWrapper {
    associatedtype Failure
    associatedtype Upstream: Publisher where Upstream.Output == UpstreamMessage, Upstream.Failure == Failure
    associatedtype Downstream: Subscriber & Cancellable where Downstream.Input == DownstreamMessage, Downstream.Failure == Upstream.Failure
    func dbPublisher() -> Upstream
    func dbSubscriber() -> Downstream
    func dropSyncObjects() throws
}
