// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "MeerkatClientCore",
    platforms: [
        .iOS("13.0"),
        .macOS("10.15")
    ],
    products: [
        .library(
            name: "MeerkatClientCore",
            targets: ["MeerkatClientCore"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/MeerkatSync/meerkatcore", from: "1.0.0"),
    ],
    targets: [
        .target(
            name: "MeerkatClientCore",
            dependencies: ["MeerkatCore"]),
        .testTarget(
            name: "MeerkatClientCoreTests",
            dependencies: ["MeerkatClientCore"]),
    ]
)
