//
//  CodableTransaction.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

extension CodableTransaction {
    public func changeFromDefaultGroup(to group: GroupID) -> CodableTransaction {
        .init(deletions: deletions, creations: creations.map { $0.changeFromDefaultGroup(to: group) }, modifications: modifications)
    }

    public func changeToDefaultGroup(from group: GroupID) -> CodableTransaction {
        .init(deletions: deletions, creations: creations.map { $0.changeToDefaultGroup(from: group) }, modifications: modifications)
    }
}
