//
//  DownstreamMessage.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

public enum DownstreamMessage {
    case diff(LogCodableDiff, pullRequest: SyncPullRequest?)
    case groupRemoved(logId: String, groupId: String)
    case groupCreated(logId: String, groupId: String)
    case subscribed(logId: String, userId: String, groupId: String)
    case unsubscribed(logId: String, userId: String, groupId: String)
    case failedGroupCreate(logId: String, groupId: String)
    case failedSubscribe(logId: String, userId: String, groupId: String)
}
