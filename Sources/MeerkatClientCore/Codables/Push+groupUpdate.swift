//
//  CodablePush.swift
//  
//
//  Created by Filip Klembara on 20/02/2020.
//

extension CodablePush {
    public func changeFromDefaultGroup(to group: GroupID) -> CodablePush {
        .init(schemeVersion: schemeVersion,
              groups: groups.map { $0.changeFromDefaultGroup(to: group) },
              transaction: transaction.changeFromDefaultGroup(to: group))
    }
}
