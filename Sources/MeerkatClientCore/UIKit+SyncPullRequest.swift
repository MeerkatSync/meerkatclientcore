//
//  UIKit+SyncPullRequest.swift
//  
//
//  Created by Filip Klembara on 02/03/2020.
//

#if canImport(UIKit)
import UIKit
import Foundation

extension SyncPullRequest {
    static var remoteNotificationRootKey: String {
        "meerkat-sync"
    }
    static var remoteNotificationKey: String {
        "pullRequest"
    }
    private static func wrapHandler(_ handler: @escaping FetchCompletionHandler) -> (NotificationFetchResult) -> Void {
        let transform = { (res: NotificationFetchResult) -> Void in
            switch res {
            case .newData:
                handler(.newData)
            case .noData:
                handler(.noData)
            case .failed:
                handler(.failed)
            }
        }
        return transform
    }

    private static func wrapHandler(request: SyncPullRequest) -> FetchCompletionHandler {
        let newHandler: FetchCompletionHandler = { status in
            let newStatus: NotificationFetchResult
            switch status {
            case .newData:
                newStatus = .newData
            case .noData:
                newStatus = .noData
            case .failed:
                newStatus = .failed
            @unknown default:
                newStatus = .noData
            }
            request.complete(newStatus, by: .user)
        }
        return newHandler
    }

    public typealias FetchCompletionHandler = (UIBackgroundFetchResult) -> Void
    public static func wrapRemoteNotification(from userInfo: [AnyHashable: Any], with fetchCompletionHandler: @escaping FetchCompletionHandler) -> FetchCompletionHandler {
        guard let root = userInfo[SyncPullRequest.remoteNotificationRootKey] as? [AnyHashable: Any],
            let sync = root[SyncPullRequest.remoteNotificationKey] as? Bool,
            sync else {

                return fetchCompletionHandler
        }
        let request = SyncPullRequest(completion: wrapHandler(fetchCompletionHandler))
        request.send()
        return wrapHandler(request: request)
    }

    public static func wrapFetch(with completionHandler: @escaping FetchCompletionHandler) -> FetchCompletionHandler {
        let request = SyncPullRequest(completion: wrapHandler(completionHandler))
        request.send()
        return wrapHandler(request: request)
    }
}
#endif

extension SyncPullRequest {
    public static func send() {
        send(completion: { _ in })
    }

    public static func send(completion: @escaping (NotificationFetchResult) -> Void) {
        let request = SyncPullRequest(completion: completion)
        request.complete(.noData, by: .user)
        request.send()
    }
}
